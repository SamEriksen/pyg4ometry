<?xml version="1.0" ?>
<gdml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://service-spi.web.cern.ch/service-spi/app/releases/GDML/schema/gdml.xsd">
	<define>
		<constant name="wbx" value="1000.0"/>
		<constant name="wby" value="1000.0"/>
		<constant name="wbz" value="1000.0"/>
		<constant name="bx" value="5.0"/>
		<constant name="by" value="bx"/>
		<constant name="bz" value="bx"/>
		<position name="center" unit="mm" x="0" y="0" z="0"/>
		<rotation name="identity" unit="rad" x="0" y="0" z="0"/>
	</define>
	<materials>
		<element Z="8" formula="O" name="Oxygen">
			<atom value="16.0"/>
		</element>
		<element Z="7" formula="N" name="Nitrogen">
			<atom value="14.01"/>
		</element>
		<material Z="13" name="Al">
			<D value="2.7"/>
			<atom value="26.98"/>
		</material>
		<material name="Air">
			<D value="1.29"/>
			<fraction n="0.7" ref="Nitrogen"/>
			<fraction n="0.3" ref="Oxygen"/>
		</material>
	</materials>
	<solids>
		<box lunit="mm" name="WorldBox" x="wbx" y="wby" z="wbz"/>
		<box lunit="mm" name="box" x="20*bx" y="20*bx" z="20*bx"/>
		<cone aunit="rad" deltaphi="2*pi" lunit="mm" name="cone_param" rmax1="0.2" rmax2="1.0" rmin1="0.1" rmin2="0.5" startphi="0" z="1"/>
	</solids>
	<structure>
		<volume name="cone1Vol">
			<materialref ref="Al"/>
			<solidref ref="cone_param"/>
		</volume>
		<volume name="box2Vol">
			<materialref ref="Air"/>
			<solidref ref="box"/>
			<paramvol ncopies="9">
				<volumeref ref="cone1Vol"/>
				<parameterised_position_size>
					<parameters number="1">
						<position name="cone1Vol1pos" unit="mm" x="0" y="0" z="-8*bx"/>
						<rotation name="cone11Vol1Rot" unit="rad" x="0" y="0" z="-0.4"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.1" lunit="mm" rmax1="0.2" rmax2="1.0" rmin1="0.1" rmin2="0.5" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="2">
						<position name="cone1Vol2pos" unit="mm" x="0" y="0" z="-6*bx"/>
						<rotation name="cone1Vol2Rot" unit="rad" x="0" y="0" z="-0.3"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.2" lunit="mm" rmax1="0.4" rmax2="1.4" rmin1="0.2" rmin2="0.6" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="3">
						<position name="cone1Vol3pos" unit="mm" x="0" y="0" z="-4*bx"/>
						<rotation name="cone1Vol3Rot" unit="rad" x="0" y="0" z="-0.2"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.3" lunit="mm" rmax1="0.6" rmax2="1.8" rmin1="0.3" rmin2="0.7" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="4">
						<position name="cone1Vol4pos" unit="mm" x="0" y="0" z="-2*bx"/>
						<rotation name="cone1Vol24ot" unit="rad" x="0" y="0" z="-0.1"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.4" lunit="mm" rmax1="0.8" rmax2="2.2" rmin1="0.4" rmin2="0.8" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="5">
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.5" lunit="mm" rmax1="1.0" rmax2="2.6" rmin1="0.5" rmin2="0.9" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="6">
						<position name="cone1Vol6pos" unit="mm" x="0" y="0" z="2*bx"/>
						<rotation name="cone1Vol6Rot" unit="rad" x="0" y="0" z="0.1"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.6" lunit="mm" rmax1="1.2" rmax2="3.0" rmin1="0.6" rmin2="1.0" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="7">
						<position name="cone1Vol7pos" unit="mm" x="0" y="0" z="4*bx"/>
						<rotation name="cone1Vol7Rot" unit="rad" x="0" y="0" z="0.2"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.7" lunit="mm" rmax1="1.4" rmax2="3.4" rmin1="0.7" rmin2="1.1" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="8">
						<position name="cone1Vol8pos" unit="mm" x="0" y="0" z="6*bx"/>
						<rotation name="cone1Vol8Rot" unit="rad" x="0" y="0" z="0.3"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.8" lunit="mm" rmax1="1.6" rmax2="3.8" rmin1="0.8" rmin2="1.2" startphi="0" z="2.5"/>
					</parameters>
					<parameters number="9">
						<position name="cone1Vol9pos" unit="mm" x="0" y="0" z="8*bx"/>
						<rotation name="cone1Vol9Rot" unit="rad" x="0" y="0" z="0.4"/>
						<cone_dimensions aunit="rad" deltaphi="2*pi*0.9" lunit="mm" rmax1="1.8" rmax2="4.2" rmin1="0.9" rmin2="1.3" startphi="0" z="2.5"/>
					</parameters>
				</parameterised_position_size>
			</paramvol>
		</volume>
		<volume name="World">
			<materialref ref="Air"/>
			<solidref ref="WorldBox"/>
			<physvol name="box2Vol_PV">
				<volumeref ref="box2Vol"/>
				<positionref ref="center"/>
				<rotationref ref="identity"/>
			</physvol>
		</volume>
	</structure>
	<setup name="Default" version="1.0">
		<world ref="World"/>
	</setup>
</gdml>
